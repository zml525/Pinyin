#Pinyin

#简介
汉字转拼音是一款小巧的汉字转拼音软件。用户只需要输入或粘贴汉字点转换(带声调)或转换(不带声调)按钮就会显示出对应的拼音,方便快捷，您的汉字小帮手，喜欢的话赶紧下载吧。

#演示
http://www.wandoujia.com/apps/com.mj.pinyin

#捐赠
开源，我们是认真的，感谢您对我们开源力量的鼓励。


![支付宝](https://git.oschina.net/uploads/images/2017/0607/164544_ef822cc0_395618.png "感谢您对我们开源力量的鼓励")
![微信](https://git.oschina.net/uploads/images/2017/0607/164843_878b9b7f_395618.png "感谢您对我们开源力量的鼓励")