package com.mj.pinyin.activity;

import java.util.Timer;
import java.util.TimerTask;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.pinyin.R;
import com.mj.pinyin.util.PinyinUtil;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
/**
 * 汉字转拼音
 * @author zhaominglei
 * @date 2015-5-2
 * 
 */
public class MainActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = MainActivity.class.getSimpleName();
	private PinyinUtil pinyinUtil = new PinyinUtil();
	private boolean isExit = false;
	private TimerTask timerTask;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private Button pasteBtn; //粘贴
	private EditText hanziText; //要转换的汉字输入框
	private String hanzi; //要转换的汉字
	private Button withtoneBtn; //转换(带声调)
	private Button notoneBtn; //转换(不带声调)
	private Button resetBtn; //清空
	private Button copyBtn; //复制结果
	private TextView resultView; //结果
	private String result; //结果
	private Button appOffersButton; //推荐应用
	private ClipboardManager clipboardManager; //剪贴板
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.miniAdLinearLayout);
		pasteBtn = (Button)findViewById(R.id.pinyin_paste_btn);
		hanziText = (EditText)findViewById(R.id.pinyin_text_edt);
		withtoneBtn = (Button)findViewById(R.id.pinyin_withtone_btn);
		notoneBtn = (Button)findViewById(R.id.pinyin_notone_btn);
		resetBtn = (Button)findViewById(R.id.pinyin_reset_btn);
		copyBtn = (Button)findViewById(R.id.pinyin_copy_btn);
		resultView = (TextView)findViewById(R.id.pinyin_result);
		appOffersButton = (Button)findViewById(R.id.appOffersButton);
		
		pasteBtn.setOnClickListener(this);
		withtoneBtn.setOnClickListener(this);
		notoneBtn.setOnClickListener(this);
		resetBtn.setOnClickListener(this);
		copyBtn.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);

		appControl = AppControl.getInstance();
		appControl.init(MainActivity.this, "84b761917ac20772/gEWh1V59OarYYiEwuSRJiVW1MiuxFgAXd9dF/kLTumaRV732w", "木蚂蚁");
		appControl.loadPopAd(MainActivity.this);
		appControl.showPopAd(MainActivity.this, 60 * 1000);
		appControl.showInter(MainActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(MainActivity.this, "84b761917ac20772/gEWh1V59OarYYiEwuSRJiVW1MiuxFgAXd9dF/kLTumaRV732w", "木蚂蚁");
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
	}
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.pinyin_paste_btn:
			clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
			String content = clipboardManager.getText().toString();
			if (content != null && !content.equals("")) {
				hanziText.setText(content);
				Toast.makeText(getApplicationContext(), R.string.pinyin_paste_success, Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(), R.string.pinyin_paste_error, Toast.LENGTH_SHORT).show();
			}
			break;
		
		case R.id.pinyin_withtone_btn:
			getPinyin("withtone");
			break;
			
		case R.id.pinyin_notone_btn:
			getPinyin("notone");
			break;
			
		case R.id.pinyin_reset_btn:
			hanziText.setText("");
			resultView.setText(R.string.pinyin_baike);
			break;
			
		case R.id.pinyin_copy_btn:
			hanzi = hanziText.getText().toString();
			if (hanzi == null || hanzi.equals("") || hanzi.trim().equals("")) {
				Toast.makeText(getApplicationContext(), R.string.pinyin_text_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			if (result != null && !result.equals("")) {
				clipboardManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
				clipboardManager.setText(result);
				Toast.makeText(getApplicationContext(), R.string.pinyin_copy_success, Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.appOffersButton:
			access.openWALL(MainActivity.this);
			break;

		default:
			break;
		}
	}
	
	private void getPinyin(String type) {
		hanzi = hanziText.getText().toString();
		if (hanzi == null || hanzi.equals("")) {
			Toast.makeText(getApplicationContext(), R.string.pinyin_text_hint, Toast.LENGTH_SHORT).show();
			return;
		}
		result = "";
		if (type != null && type.equals("withtone")) {
			result = pinyinUtil.getPinyinWithTone(hanzi);
		} else if (type != null && type.equals("notone")) {
			result = pinyinUtil.getPinyinNoTone(hanzi);
		} else {
			result = hanzi;
		}
		resultView.setText(result);
	}

	@Override
	public void onBackPressed() {
		if (isExit) {
			MainActivity.this.finish();
		} else {
			isExit = true;
			Toast.makeText(MainActivity.this, R.string.exit_msg, Toast.LENGTH_SHORT).show();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			};
			new Timer().schedule(timerTask, 2*1000);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}
}
